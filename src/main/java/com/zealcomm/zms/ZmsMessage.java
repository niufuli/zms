package com.zealcomm.zms;

public class ZmsMessage {

    public static final String ZMS_JOIN_SUCCESS = "zms_join_success";
    public static final String ZMS_JOIN_FAILED = "zms_join_failed";

    public static final String ZMS_PUBLISH_SUCCESS = "zms_publish_success";
    public static final String ZMS_PUBLISH_FAILED = "zms_publish_failed";

    public static final String ZMS_SUBSCRIBE_SUCCESS = "zms_subscribe_success";
    public static final String ZMS_SUBSCRIBE_FAILED = "zms_subscribe_failed";
}
