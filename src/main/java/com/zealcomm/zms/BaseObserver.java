package com.zealcomm.zms;

import android.util.Log;

import java.util.HashMap;

import irtc.base.RemoteStream.StreamObserver;
import irtc.conference.Participant;
import irtc.conference.RemoteStream;

public class BaseObserver {

    private  final String TAG = this.getClass().getName();

    BaseObserverCallback baseObserverCallback;
    HashMap<Participant, Participant.ParticipantObserver> participantObservers = new HashMap<>();
    HashMap<RemoteStream, StreamObserver> remoteStreamObservers = new HashMap<>();
    public BaseObserver(BaseObserverCallback baseObserverCallback) {
        this.baseObserverCallback = baseObserverCallback;
    }

    public void addRemoteStream(final RemoteStream remoteStream){

        if(remoteStreamObservers.get(remoteStream) == null){
            StreamObserver streamObserver  = new StreamObserver() {
                @Override
                public void onEnded() {
                    baseObserverCallback.streamEnd(remoteStream);
                }

                @Override
                public void onUpdated() {
                    baseObserverCallback.streamUpdate(remoteStream);
                }
            };
            remoteStream.addObserver(streamObserver);
            remoteStreamObservers.put(remoteStream,streamObserver);
        }
    }

    public void addParticipant(final Participant participant){

        if(participantObservers.get(participant) == null){
            Participant.ParticipantObserver participantObserver  = new Participant.ParticipantObserver() {
                @Override
                public void onLeft() {
                    baseObserverCallback.participantLeft(participant);
                }
            };
            participantObservers.put(participant,participantObserver);
        }
    }

    public void removeStream(final RemoteStream remoteStream){
        StreamObserver observer = remoteStreamObservers.get(remoteStream);
        if(observer!= null){
            Log.i(TAG,"remove stream");
            remoteStream.removeObserver(observer);
            remoteStreamObservers.remove(remoteStream);
        }
    }

    public void removeParticipant(final Participant participant){
        Participant.ParticipantObserver observer = participantObservers.get(participant);
        if(observer!= null){
            Log.i(TAG,"remove participant");
            participant.removeObserver(observer);
            participantObservers.remove(participant);
        }
    }

    public interface BaseObserverCallback{

        void streamEnd(RemoteStream remoteStream);

        void streamUpdate(RemoteStream remoteStream);

        void participantLeft(Participant participant);

    }
}
