package com.zealcomm.zms;

import irtc.conference.RemoteStream;
import irtc.conference.Subscription;

public class SubSuccessData {

    private RemoteStream remoteStream;
    private Subscription subscription;

    public RemoteStream getRemoteStream() {
        return remoteStream;
    }

    public void setRemoteStream(RemoteStream remoteStream) {
        this.remoteStream = remoteStream;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
}
