package com.zealcomm.zms;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;


import com.zealcomm.app.util.LogUtil;
import com.zealcomm.base.Constants;
import com.zealcomm.base.MessageData;
import com.zealcomm.base.TLSSocketFactory;

import org.webrtc.EglBase;
import org.webrtc.EglRenderer;
import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import irtc.base.ActionCallback;
import irtc.base.ContextInitialization;
import irtc.base.IRtcError;
import irtc.base.LocalStream;
import irtc.base.MediaConstraints;
import irtc.base.VideoCodecParameters;
import irtc.conference.ConferenceClient;
import irtc.conference.ConferenceClientConfiguration;
import irtc.conference.ConferenceInfo;
import irtc.conference.Participant;
import irtc.conference.Publication;
import irtc.conference.PublishOptions;
import irtc.conference.RemoteStream;
import irtc.conference.SubscribeOptions;
import irtc.conference.Subscription;

import static irtc.base.MediaCodecs.VideoCodec.H264;
import static irtc.base.MediaCodecs.VideoCodec.VP8;

public class ZmsClient implements ConferenceClient.ConferenceClientObserver{

    private final String TAG = getClass().getName();
    private ConferenceClient mConferenceClient;
    private static ZmsClient mZmsClient;
    private EglBase rootEglBase;
    private Context mContext;
    public IRtcVideoCapturer mCapturer;
    private IRtcScreenCapturer mIRtcScreenCapturer;
    private ZmsEventMessage mZmsEventMessage;
    private LocalStream mLocalStream,mScreenStream;
    private Publication mPublication,mScreenPublication;
    private SurfaceViewRenderer mLocalRenderer;
    private ConferenceInfo mConferenceInfo;
    List<RemoteStream> subSuccessList = new ArrayList<>();
    private String mMixStreamId,mPublishStreamId,mSelfId;
    private Boolean mIfSubSelf;

    public String getMixStreamId() {
        return mMixStreamId;
    }

    public String getPublishStreamId() {
        return mPublishStreamId;
    }

    public String getSelfId() {
        return mSelfId;
    }

    public void setIfSubSelf(Boolean ifSubSelf) {mIfSubSelf = ifSubSelf;}

    private ZmsClient (){
        mIfSubSelf = false;
    }

    public static ZmsClient getInstance(){
        if(mZmsClient == null){
            mZmsClient = new ZmsClient();
        }
        return mZmsClient;
    }

    public void init(Context context,ZmsEventMessage zmsEventMessage){
        mZmsEventMessage = zmsEventMessage;
        mContext = context;
        rootEglBase = EglBase.create();
        ContextInitialization.create()
                .setApplicationContext(mContext)
                .setVideoHardwareAccelerationOptions(
                        rootEglBase.getEglBaseContext(),
                        rootEglBase.getEglBaseContext())
                .initialize();
        ConferenceClientConfiguration configuration
                = ConferenceClientConfiguration.builder()
                .setHostnameVerifier(TLSSocketFactory.mHostnameVerifier)
                .setSSLContext(TLSSocketFactory.sslContext)
                .build();
        mConferenceClient = new ConferenceClient(configuration);
        mConferenceClient.addObserver(this);
    }

    public  EglBase.Context getEGLContext(){
        return rootEglBase.getEglBaseContext();
    }

    public void joinRoom(String token){
        LogUtil.i("joinRoom and token:" + token);
        MessageData messageData = new MessageData();
        mConferenceClient.join(token, new ActionCallback<ConferenceInfo>() {
            @Override
            public void onSuccess(ConferenceInfo conferenceInfo) {
                mConferenceInfo = conferenceInfo;
                mSelfId = mConferenceInfo.self().id;
                messageData.setType(ZmsMessage.ZMS_JOIN_SUCCESS);
                mZmsEventMessage.onZmsEvent(messageData);
                onStreamAdded(null);
            }

            @Override
            public void onFailure(IRtcError e) {
                messageData.setType(ZmsMessage.ZMS_JOIN_FAILED);
                mZmsEventMessage.onZmsEvent(messageData);
            }
        });
    }

    public void publish(int width, int height, int fps,String type,PublishOptions options){
        LogUtil.i("ZmsClient publish stream");
        mCapturer = IRtcVideoCapturer.create(width,height, fps, true);
        mLocalStream = new LocalStream(mCapturer,
                new MediaConstraints.AudioTrackConstraints());
        HashMap<String,String> att = new HashMap<>();
        att.put(Constants.STREAM_TYPE,type);
        mLocalStream.setAttributes(att);
        if(mLocalRenderer != null){
            mLocalStream.attach(mLocalRenderer);
        }
        MessageData messageData = new MessageData();
        ActionCallback<Publication> callback = new ActionCallback<Publication>() {
            @Override
            public void onSuccess(final Publication result) {
                mPublication = result;
            }

            @Override
            public void onFailure(final IRtcError error) {
                messageData.setType(ZmsMessage.ZMS_PUBLISH_FAILED);
                mZmsEventMessage.onZmsEvent(messageData);
            }
        };
        mConferenceClient.publish(mLocalStream, options, callback);
    }

    public void setRenderer(SurfaceViewRenderer renderer){
        mLocalRenderer = renderer;
        if(mLocalStream != null){
            mLocalStream.attach(mLocalRenderer);
        }
    }

    public void screenShare(Intent data,int width, int height){
        screenShare(data, width, height, "screen");
    }

    public void screenShare(Intent data,int width, int height, String type){
        LogUtil.i("ZmsClient.screenShare() publish steam");
        mIRtcScreenCapturer = new IRtcScreenCapturer(data, width,height);
        mScreenStream = new LocalStream(mIRtcScreenCapturer,new MediaConstraints.AudioTrackConstraints());
        HashMap<String,String> att = new HashMap<>();
        att.put("type", type);
        mScreenStream.setAttributes(att);
        mConferenceClient.publish(mScreenStream, new ActionCallback<Publication>() {
            @Override
            public void onSuccess(Publication result) {
                Log.e(TAG,"mScreenStream success");
                mScreenPublication = result;
            }

            @Override
            public void onFailure(IRtcError error) {
                Log.e(TAG,"IRtcError error"+error.errorMessage);
                mIRtcScreenCapturer.stopCapture();
                mIRtcScreenCapturer.dispose();
                mIRtcScreenCapturer = null;
                mScreenStream.dispose();
                mScreenStream = null;
            }
        });
    }

    public void stopScreen(){
        if (mScreenPublication != null) {
            mScreenPublication.stop();
            mScreenPublication = null;
        }
        if(mScreenStream != null){
            mScreenStream.dispose();
            mScreenStream = null;
        }
        if(mIRtcScreenCapturer != null) {
            mIRtcScreenCapturer.stopCapture();
            mIRtcScreenCapturer.dispose();
            mIRtcScreenCapturer = null;
        }
    }

    public void setLocalAudio(boolean isMute){
        if(isMute) {
            if (mLocalStream != null) {
                mLocalStream.disableAudio();
            }
            if (mPublication != null) {
                mPublication.mute(MediaConstraints.TrackKind.AUDIO, null);
            }
        }else {
            if (mLocalStream != null) {
                mLocalStream.enableAudio();
            }
            if (mPublication != null) {
                mPublication.unmute(MediaConstraints.TrackKind.AUDIO, null);
            }
        }
    }

    public void setLocalVideo(boolean isMute){
        if(isMute) {
            if (mLocalStream != null) {
                mLocalStream.disableVideo();
            }
            if (mPublication != null) {
                mPublication.mute(MediaConstraints.TrackKind.VIDEO, null);
            }
        }else {
            if (mLocalStream != null) {
                mLocalStream.enableVideo();
            }
            if (mPublication != null) {
                mPublication.unmute(MediaConstraints.TrackKind.VIDEO, null);
            }
        }
    }

    public void stop(){
        if(mPublication != null) {
            mPublication.stop();
        }
        if(mLocalStream != null && mLocalStream.hasVideo()) {
            mLocalStream.detach(mLocalRenderer);
        }
        if (mCapturer != null) {
            mCapturer.stopCapture();
            mCapturer.dispose();
            mCapturer = null;
        }
        if(mLocalStream != null) {
            mLocalStream.dispose();
            mLocalStream = null;
        }
        subSuccessList.clear();
        mConferenceClient.leave();
    }

    public void drop(){
        if(mLocalStream != null && mLocalStream.hasVideo()) {
            mLocalStream.detach(mLocalRenderer);
        }
        if (mCapturer != null) {
            mCapturer.stopCapture();
            mCapturer.dispose();
            mCapturer = null;
        }
        if(mLocalStream != null) {
            mLocalStream.dispose();
            mLocalStream = null;
        }
        subSuccessList.clear();
    }

    public void switchCamera(){
        mCapturer.switchCamera();
    }

    @Override
    public void onStreamAdded(RemoteStream stream) {
        //订阅所有流
        if (mConferenceInfo != null) {
            for (RemoteStream remoteStream : mConferenceInfo.getRemoteStreams()) {
                if (!remoteStream.id().contains("common")) {
                    if(remoteStream.getAttributes() != null) {
                        String streamType = remoteStream.getAttributes().get("type");
                        if(remoteStream.origin().equals(mConferenceInfo.self().id) && (!mIfSubSelf)){
                            continue;
                        }
                        boolean isAlreadySub = false;
                        for (RemoteStream item : subSuccessList) {
                            if (item.id().equals(remoteStream.id())) {
                                isAlreadySub = true;
                            }
                        }
                        if (!isAlreadySub) {
                            subSuccessList.add(remoteStream);
                            SubscribeOptions.VideoSubscriptionConstraints videoOption =
                                    SubscribeOptions.VideoSubscriptionConstraints.builder()
                                            .addCodec(new VideoCodecParameters(H264))
                                            .addCodec(new VideoCodecParameters(VP8))
                                            .build();
                            SubscribeOptions.Builder optionsBuilder = SubscribeOptions.builder(false,true);
                            optionsBuilder.setVideoOption(videoOption);
                            if("screen".equals(streamType)) {
                                subscribeRemoteStream(remoteStream, optionsBuilder.build());
                            }else {
                                subscribeRemoteStream(remoteStream,null);
                            }
                        }

                    }
                }else {
                    mMixStreamId = remoteStream.id();
                }
            }
            if(stream != null && stream.origin().equals(mConferenceInfo.self().id)){
                mPublishStreamId = stream.id();
                MessageData messageData = new MessageData();
                messageData.setType(ZmsMessage.ZMS_PUBLISH_SUCCESS);
                mZmsEventMessage.onZmsEvent(messageData);
                return;
            }
        } else {
            Log.d("RoomActivity", "mConferenceInfo is null");
        }
    }

    protected void subscribeRemoteStream(RemoteStream remoteStream, SubscribeOptions options) {
        mConferenceClient.subscribe(remoteStream, options,new ActionCallback<Subscription>() {
            @Override
            public void onSuccess(Subscription result) {
                Log.e(TAG, "Success to subscribe ="+result.id);
                SubSuccessData subSuccessData = new SubSuccessData();
                subSuccessData.setRemoteStream(remoteStream);
                subSuccessData.setSubscription(result);
                MessageData messageData = new MessageData();
                messageData.setData(subSuccessData);
                messageData.setType(ZmsMessage.ZMS_SUBSCRIBE_SUCCESS);
                mZmsEventMessage.onZmsEvent(messageData);
            }

            @Override
            public void onFailure(IRtcError error) {
                Log.e(TAG, "Failed to subscribe "
                        + error.errorMessage);
            }
        });
    }

    @Override
    public void onParticipantJoined(Participant participant) {

    }

    @Override
    public void onMessageReceived(String s, String s1, String s2) {

    }

    @Override
    public void onServerDisconnected() {

    }

    public interface ZmsEventMessage{
        void onZmsEvent(MessageData messageData);
    }
}
