package com.zealcomm.zms;

import android.annotation.TargetApi;
import android.content.Intent;
import android.media.projection.MediaProjection;
import android.os.Build;

import org.webrtc.ScreenCapturerAndroid;

import irtc.base.Stream;
import irtc.base.VideoCapturer;


public class IRtcScreenCapturer extends ScreenCapturerAndroid implements VideoCapturer {
    private int width, height;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IRtcScreenCapturer(Intent data, int width, int height) {
        super(data, new MediaProjection.Callback() {
            @Override
            public void onStop() {
                super.onStop();
            }
        });
        this.width = width;
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getFps() {
        // ignored
        return 0;
    }

    @Override
    public Stream.StreamSourceInfo.VideoSourceInfo getVideoSource() {
        return Stream.StreamSourceInfo.VideoSourceInfo.SCREEN_CAST;
    }
}
